using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class UIEventHandler : MonoBehaviour
{
    [SerializeField]
    private string buttonName;
    [SerializeField]
    private string sceneName;

    private UIDocument _uiDocument;
    private VisualElement _button;
    // Start is called before the first frame update
    void Awake()
    {
        _uiDocument = FindObjectOfType<UIDocument>();
        _button = _uiDocument.rootVisualElement.Query<Button>(buttonName);
    }

    void OnEnable(){
        _button.RegisterCallback<ClickEvent>(OnMouseDownEvent);
    }

    public void OnMouseDownEvent(ClickEvent evt){
        SceneManager.LoadSceneAsync(sceneName);
    }
    // Update is called once per frame
    void OnDisable(){
        _button.UnregisterCallback<ClickEvent>(OnMouseDownEvent);
    }
}
