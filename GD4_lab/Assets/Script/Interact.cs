using UnityEngine;
using UnityEngine.Events;

namespace Script
{
    public class Interact : MonoBehaviour
    {
        private bool isCollide;
        public UnityEvent OnInteract;
        public UnityEvent OnUninteract;
        private bool _isInteract;
        void Update()
        {
            if (isCollide)
            {
                if (Input.GetKeyDown(KeyCode.E))
                {
                    _isInteract = !_isInteract;
                }
            }

            if (_isInteract)
            {
                OnInteract.Invoke();
            }
            else if (!_isInteract)
            {
                OnUninteract.Invoke();
            }
        }

        public void SetCollide(bool value)
        {
            isCollide = value;
        }
    }
}