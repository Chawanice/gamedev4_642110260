using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour
{
    private PlayerInput _playerInput;
    private float hor, ver;
    public TextMeshProUGUI _text;
    
    public void OnMovement(InputValue value)
    {
        hor = value.Get<Vector2>().x;
        ver = value.Get<Vector2>().y;
    }

    public void Update()
    {
        if (_text)
        {
            if (hor != 0 || ver != 0)
            {
                _text.text = hor +":"+ ver;
            }
        }
    }


}
